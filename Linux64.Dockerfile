FROM ubuntu:jammy

ENV RUST_VERSION="nightly-2023-04-14"

RUN apt-get update && apt-get -y install wget make valgrind binutils-dev libdw-dev cmake
RUN apt-get -y install clang lld python3 python3-pip clang-format ninja-build doxygen git curl jq

COPY run-clang-format.py /usr/bin/run-clang-format.py

# Build mold
RUN apt-get -y install mold

# Install Rust nightly
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain none -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN rustup toolchain install ${RUST_VERSION} --allow-downgrade --profile minimal --component clippy
RUN rustup override set ${RUST_VERSION}
RUN rustup target add wasm32-unknown-unknown

# Install Rust Miri
RUN rustup component add miri
# Install Rust nextest
RUN curl -LsSf https://get.nexte.st/latest/linux | tar zxf - -C ${CARGO_HOME:-~/.cargo}/bin
# Install Cargo LLVM coverage
RUN cargo install cargo-llvm-cov

RUN rustup component add rustfmt
RUN rustup component add llvm-tools-preview
RUN rustup component add rust-src

RUN apt-get install -y llvm-12-dev libclang-common-12-dev
ENV LLVM_SYS_120_STRICT_VERSIONING=0